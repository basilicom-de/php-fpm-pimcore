#!/usr/bin/env bash

# Setup a cron schedule
echo "$STARTUP_CRON
# This extra line makes it a valid cron" > /tmp/scheduler.txt

# better cron for containers: https://github.com/aptible/supercronic
/usr/local/bin/supercronic /tmp/scheduler.txt

