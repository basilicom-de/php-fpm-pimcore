#!/usr/bin/env bash

if [[ -z "$SKIP_MYSQL_CHECK" ]]; then
  echo "Waiting for MySQL on host [$MYSQL_HOST] port [$MYSQL_PORT]:";
  while ! nc -z $MYSQL_HOST $MYSQL_PORT;
    do
      sleep 2;
      echo "Still waiting...";
    done;
  echo 'OK - MySQL Server is up!';
fi

if [[ -n "$STARTUP_CRON" ]]; then
  . /startup-cron.sh
elif [[ -n "$STARTUP_CLI" ]]; then
  . /startup-cli.sh
else
  . /startup-fpm.sh
fi