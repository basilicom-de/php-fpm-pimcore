#!/usr/bin/env bash

echo "Running Pimcore install command [$PIMCORE_INSTALL_COMMAND] ...";
su -s /bin/bash www-data -c "$PIMCORE_INSTALL_COMMAND";
echo 'OK - php-fpm-pimcore startup complete.';

echo "Finally starting php-fpm: ";

php-fpm

